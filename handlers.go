package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)


func home() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, gin.H{"routes": router.Arr()})
	}
}

func bindQueryName() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		bn := BindName{}

		if err := ctx.ShouldBindQuery(&bn); err != nil {
			ctx.JSON(http.StatusBadRequest,response{Error: "cannot bind query params" + err.Error()})
		}

		ctx.JSON(http.StatusOK, bn)
	}
}

func bindURIName() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		bn := BindName{}

		if err := ctx.ShouldBindUri(&bn); err != nil {
			ctx.JSON(http.StatusBadRequest,response{Error: "cannot bind URI params" + err.Error()})
		}

		ctx.JSON(http.StatusOK, bn)
	}
}

func bindPostName() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		bn := BindName{}

		if err := ctx.ShouldBind(&bn); err != nil {
			ctx.JSON(http.StatusBadRequest,response{Error: "cannot bind POST params" + err.Error()})
		}

		ctx.JSON(http.StatusOK, bn)
	}
}
