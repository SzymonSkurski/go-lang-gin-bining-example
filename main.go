package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type BindName struct {
	NameP string `json:"nameP" form:"name"`
	NameQ string `json:"nameQ" query:"name"`
	NameU string `json:"nameU" uri:"name"`
}

type response struct {
	Error string `json:"error,omitempty"`
	Msg string `json:"msg,omitempty"`
}


func main() {
	engine := gin.New()
	engine.GET("/", home())

	for _, sr := range router {
		switch sr.method {
		case http.MethodGet: engine.GET(sr.path, sr.handler)
		case http.MethodPost: engine.POST(sr.path, sr.handler)
		case http.MethodPatch: engine.PATCH(sr.path, sr.handler)
		case http.MethodPut: engine.PUT(sr.path, sr.handler)
		case http.MethodDelete: engine.DELETE(sr.path, sr.handler)
		default: panic(fmt.Errorf("router unsupported method: %s", sr.method))
		}
	}
	
	host := "localhost"
	port := 8085
	addr := fmt.Sprintf("%s:%d", host, port)

	fmt.Printf("\nhttp://%s\n\n", addr)
	if err := engine.Run(addr); err != nil {
		fmt.Println(err)

		return
	}
}
