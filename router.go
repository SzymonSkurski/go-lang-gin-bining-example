package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

//	engine.POST("/test", bindPostName())
//	engine.GET("/test/:name", bindURIName())
//	engine.GET("/test", bindQueryName())

var router = routes{
	{
		path:   "/test",
		desc:   "Bind form name param",
		method: http.MethodPost,
		handler: bindPostName(),
	},
	{
		path:   "/test/:name",
		desc:   "Bind name from URI param, for example test/john",
		method: http.MethodGet,
		handler: bindURIName(),
	},
	{
		path:   "/test",
		desc:   "Bind name from query param, for example test?name=john",
		method: http.MethodGet,
		handler: bindQueryName(),
	},
}

type route struct{
	path string
	desc string
	method string
	handler gin.HandlerFunc
}

type routeToHTTP struct {
	Path string `json:"path"`
	Desc string `json:"desc"`
}

func (r route) toHTTP() routeToHTTP {
	return routeToHTTP{
		Path: fmt.Sprintf("(%s) %s", r.method, r.path),
		Desc: r.desc,
	}
}

type routes []route

func (r routes) Arr() []routeToHTTP {
	str := make([]routeToHTTP, len(r))

	for i, st := range r {
		str[i] = st.toHTTP()
	}

	return str
}
